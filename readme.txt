Curso de Udemy de paga "Diseño Web Desde Cero a Avanzado 45h" por Jose Javier Villena
Duración: 45 horas

Herramientas:
* Comprimir y descomprimir CSS: https://herramientas-online.com/comprimir-descomprimir-css.html
* https://herramientas-online.com/comprimir-descomprimir-javascript.html
* Ver compatibilidad de algunas cosas con otros navegadores (soporte): https://caniuse.com/
* https://icomoon.io/

Atajos sublime text: 
- Ctrl + Shift + c: para activar el paquete instalado Colorpicker y así poner un color

- Etiquetas HTML: etiqueta + TAB
- Elementos que van a estar dentro de la misma linea con un padre: 
artile + section + aside + TAB
- Elementos dentro de otros: padre > hijo > nieto + TAB
- Varias opciones: etiqueta * N + TAB
- (>+...)>+...: figure > (img+figcaption) + TAB
- #identificador + TAB
- .clase + TAB
- etiqueta#identificador.clase + TAB
- text$$ + TAB
- etiqueta{contenido} + TAB
- etiqueta[atr=valor] + TAB


