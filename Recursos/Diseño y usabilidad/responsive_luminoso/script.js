window.addEventListener('devicelight', function(e) {
  var lux = e.value;
  document.getElementById("sensor").innerHTML = lux + " lux";

  if (lux < 50) { // luz tenue
    document.body.className = 'tenue';
    document.getElementById("modo").innerHTML = "Tenue";
  } 
  if (lux >= 50 && lux <= 1500) { //luz normal
    document.body.className = 'normal';
    document.getElementById("modo").innerHTML = "Normal";
  }
  if (lux > 1500)  { // mucha luz
    document.body.className = 'luminoso';
    document.getElementById("modo").innerHTML = "Luminoso";
  } 
});